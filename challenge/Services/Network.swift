//
//  Network.swift
//  challenge
//
//  Created by Abraao Nascimento on 13/09/20.
//  Copyright © 2020 Digio. All rights reserved.
//

import UIKit

class Network {
    private let url = "https://7hgi9vtkdc.execute-api.sa-east-1.amazonaws.com/sandbox/products"
    private let request = ABRequest()
    
    var onSpolight: (([Spotlight]) -> ())?
    var onProducts: (([Product]) -> ())?
    var onCash: ((Cash) -> ())?
    
    func load() {
        
        request.send(url: url) { (data) in
            guard let data = data else { return }
            
            do {
                if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                    
                    //
                    if let spotlightJson = json["spotlight"] as? [[String: Any]] {
                        self.parserSpotlight(spotlightJson)
                    }
                    
                    //
                    if let productsJson = json["products"] as? [[String: Any]] {
                        self.parserProducts(productsJson)
                    }
                    
                    //
                    if let cash = json["cash"] as? [String: Any] {
                        let cash = Cash(json: cash)
                        if let onCash = self.onCash {
                            onCash(cash)
                        }
                    }
                    
                }
            } catch let err {
                print("[Network]: error \(err)")
            }
            
        }
        
    }
    
    private func parserSpotlight(_ spotlightJson: [[String: Any]]?) {
                       
        var spotlight = [Spotlight]()

        spotlightJson?.forEach { (spot) in
            spotlight.append(Spotlight(json: spot))
        }
        
        if let onSpotLight = self.onSpolight {
            onSpotLight(spotlight)
        }
    }
    
    private func parserProducts(_ productsJson: [[String: Any]]?) {
                       
        var products = [Product]()

        productsJson?.forEach { (prod) in
            products.append(Product(json: prod))
        }
        
        if let onProducts = self.onProducts {
            onProducts(products)
        }
    }
}
