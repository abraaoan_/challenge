//
//  Cash.swift
//  challenge
//
//  Created by Abraao Nascimento on 13/09/20.
//  Copyright © 2020 Digio. All rights reserved.
//

import UIKit

class Cash: NSObject {
    var title: String?
    var bannerUrl: String?
    var image: UIImage?
    var desc: String?
    
    init(json: [String: Any]?) {
        super.init()
        
        self.parser(json)
        
    }
    
    private func parser(_ json: [String: Any]?) {
        
        if let title = json?["title"] as? String {
            self.title = title
        }
        
        if let bannerUrl = json?["bannerURL"] as? String {
            self.bannerUrl = bannerUrl
        }
        
        if let description = json?["description"] as? String {
            self.desc = description
        }
    }
    
}
