//
//  Product.swift
//  challenge
//
//  Created by Abraao Nascimento on 13/09/20.
//  Copyright © 2020 Digio. All rights reserved.
//

import UIKit

class Product: NSObject {
    var name: String?
    var imageUrl: String?
    var image: UIImage?
    var desc: String?
    
    init(json: [String: Any]?) {
        super.init()
        
        self.parser(json)
        
    }
    
    
    private func parser(_ json: [String: Any]?) {
        
        if let name = json?["name"] as? String {
            self.name = name
        }
        
        if let imageURL = json?["imageURL"] as? String {
            self.imageUrl = imageURL
        }
        
        if let description = json?["description"] as? String {
            self.desc = description
        }
    }
    
}
