//
//  ItemCell.swift
//  challenge
//
//  Created by Abraao Nascimento on 13/09/20.
//  Copyright © 2020 Digio. All rights reserved.
//

import UIKit

class ItemCell: UICollectionViewCell {
    
    @IBOutlet var bgView: UIView!
    @IBOutlet var imageView: UIImageView!
    
    private let radius: CGFloat = 10;
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.bgView.layer.cornerRadius = radius
        self.imageView.layer.cornerRadius = radius
        self.bgView.addShadow(radius)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.imageView.image = nil
    }

}
