//
//  ProductCell.swift
//  challenge
//
//  Created by Abraao Nascimento on 16/09/20.
//  Copyright © 2020 Digio. All rights reserved.
//

import UIKit

class ProductCell: UITableViewCell {

    @IBOutlet var title: UILabel!
    @IBOutlet var collectionView: UICollectionView!
    var products: [Product]? {
        didSet {
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
    }
    var onSelectItem: ((Product) -> ())?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}

extension ProductCell: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "idItem", for: indexPath) as! ItemCell
            
        guard let product = products?[indexPath.row] else { return cell }
            
        if (product.image != nil) {
            cell.imageView.image = product.image
        } else {
            ABRequest.downloadImage(url: product.imageUrl ?? "") { (image) in
                product.image = image
                DispatchQueue.main.async {
                    if (image?.size.width ?? 0) > 0 {
                        cell.imageView.image = product.image
                    }
                }
            }
        }
        
        return cell
    }
}

extension ProductCell: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
     
        guard let product = products?[indexPath.row] else { return }
        
        if let onSelectItem = self.onSelectItem {
            onSelectItem(product)
        }
    }
}

