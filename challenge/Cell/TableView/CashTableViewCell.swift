//
//  HomeTableViewCell.swift
//  challenge
//
//  Created by Abraao Nascimento on 13/09/20.
//  Copyright © 2020 Digio. All rights reserved.
//

import UIKit

class CashTableViewCell: UITableViewCell {
    
    @IBOutlet var title: UILabel!
    @IBOutlet var cashImageView: UIImageView!
    @IBOutlet var bgImageView: UIView!
    var cash: Cash? {
        didSet {
            
            if self.cash?.image != nil {
                self.cashImageView.image = self.cash?.image
            } else {
                ABRequest.downloadImage(url: self.cash?.bannerUrl ?? "") { (image) in
                    self.cash?.image = image
                    DispatchQueue.main.async {
                        self.cashImageView.image = self.cash?.image
                    }
                }
            }
        }
    }
    
    let radius: CGFloat = 10

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        bgImageView.layer.cornerRadius = radius
        cashImageView.layer.cornerRadius = radius
        self.bgImageView.addShadow(radius)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
