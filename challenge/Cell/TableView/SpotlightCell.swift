//
//  SpotlightCell.swift
//  challenge
//
//  Created by Abraao Nascimento on 16/09/20.
//  Copyright © 2020 Digio. All rights reserved.
//

import UIKit

class SpotlightCell: UITableViewCell {

    @IBOutlet var title: UILabel!
    @IBOutlet var collectionView: UICollectionView!
    var spotlight: [Spotlight]? {
        didSet {
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
    }
    var onSelectItem: ((Spotlight) -> ())?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}



extension SpotlightCell: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return spotlight?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "idItem", for: indexPath) as! ItemCell
            
        guard let spot = spotlight?[indexPath.row] else { return cell }
            
        if (spot.image != nil) {
            cell.imageView.image = spot.image
        } else {
            ABRequest.downloadImage(url: spot.bannerUrl ?? "") { (image) in
                DispatchQueue.main.async {
                    spot.image = image
                    cell.imageView.image = image
                }
            }
        }
        
        return cell
    }
}

extension SpotlightCell: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
     
        guard let spot = spotlight?[indexPath.row] else { return }
        
        if let onSelectItem = self.onSelectItem {
            onSelectItem(spot)
        }
    }
}
