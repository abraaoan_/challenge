//
//  ViewController.swift
//  challenge
//
//  Created by Abraao Nascimento on 13/09/20.
//  Copyright © 2020 Digio. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    enum Section: Int {
        case spotlight
        case cash
        case products
        
        static var count: Int {
            var max = 0
            while let _ = Section(rawValue: max) { max += 1 }
            return max
        }
        
        func getIdentifier() -> String {
            switch self {
            case .cash:
                return "idCash"
            case .spotlight:
                return "idSpotlight"
            case .products:
                return "idProduct"
            }
        }
        
    }
    
    let service = Network()
    @IBOutlet var tableView: UITableView!
    
    var products: [Product]? {
           didSet {
               DispatchQueue.main.async {
                   let index: IndexSet = [Section.products.rawValue]
                   self.tableView.reloadSections(index, with: .fade)
               }
           }
       }
    var spotilight: [Spotlight]? {
           didSet {
               DispatchQueue.main.async {
                   let index: IndexSet = [Section.spotlight.rawValue]
                   self.tableView.reloadSections(index, with: .fade)
               }
           }
       }
    var cash: Cash? {
        didSet {
            DispatchQueue.main.async {
                let index: IndexSet = [Section.cash.rawValue]
                self.tableView.reloadSections(index, with: .fade)
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Clean separators
        tableView.tableFooterView = UIView(frame: .zero)
        
        self.service.load()
        
        self.service.onSpolight = { (spotlights) in
            self.spotilight = spotlights
        }
        
        self.service.onProducts = { (products) in
            self.products = products
        }
        
        self.service.onCash = { (cash) in
            self.cash = cash
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
    }
    
    func heightForSection(section: Section) -> CGFloat {
        switch section {
        case .spotlight:
            return 260
        case .products:
            return 188
        case .cash:
            return 168
        }
    }
    
    func navigationToDetail(image: UIImage?, description: String?) {
        let story = UIStoryboard(name: "Main", bundle: nil)
        let viewController = story.instantiateViewController(identifier: "idDetail")
        if let detailView = viewController as? DetailViewController {
            detailView.descr = description
            detailView.image = image
            self.navigationController?.pushViewController(detailView, animated: true)
        }
    }

}

extension ViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return Section.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let section = Section(rawValue: indexPath.section) else { return UITableViewCell() }
        let cell = tableView.dequeueReusableCell(withIdentifier: section.getIdentifier(), for: indexPath)
        
        switch section {
        case .spotlight:
            guard let cell = cell as? SpotlightCell else { return UITableViewCell() }
            cell.title.text = "Olá, Maria";
            cell.spotlight = spotilight
            
            //Navigation
            cell.onSelectItem = { (spotilight) in
                self.navigationToDetail(image: spotilight.image,
                                        description: spotilight.desc)
            }
            
            return cell
        case .products:
            
            guard let cell = cell as? ProductCell else { return UITableViewCell() }
            cell.title.text = "Produtos";
            cell.products = products
            
            //Navigation
            cell.onSelectItem = { (product) in
                self.navigationToDetail(image: product.image,
                                        description: product.desc)
            }
            
            return cell
        case .cash:
            guard let cell = cell as? CashTableViewCell else { return UITableViewCell() }
            cell.cash = cash
            return cell
        }
    }
}

extension ViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let section = Section(rawValue: indexPath.section) else { return }
        
        if section == .cash {
            navigationToDetail(image: cash?.image, description: cash?.desc)
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
            guard let section = Section(rawValue: indexPath.section) else { return 0 }
            return heightForSection(section: section)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard let section = Section(rawValue: indexPath.section) else { return 0 }
        return heightForSection(section: section)
    }
}
