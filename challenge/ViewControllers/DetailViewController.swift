//
//  DetailViewController.swift
//  challenge
//
//  Created by Abraao Nascimento on 17/09/20.
//  Copyright © 2020 Digio. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var label: UILabel!
    
    var image: UIImage?
    var descr: String?

    override func viewDidLoad() {
        super.viewDidLoad()

        self.imageView.image = image
        self.label.text = descr
        
        self.navigationItem.title = "Detail"
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
