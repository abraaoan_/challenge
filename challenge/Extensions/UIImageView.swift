//
//  UIImageView.swift
//  challenge
//
//  Created by Abraao Nascimento on 13/09/20.
//  Copyright © 2020 Digio. All rights reserved.
//

import UIKit

extension UIImageView {
    func downloadImage(url: String) {
        ABRequest.downloadImage(url: url) { (image) in
            if let image = image {
                DispatchQueue.main.async {
                    self.image = image
                }
            }
        }
    }
}
