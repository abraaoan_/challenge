//
//  UIView.swift
//  challenge
//
//  Created by Abraao Nascimento on 17/09/20.
//  Copyright © 2020 Digio. All rights reserved.
//

import UIKit

extension UIView {
    func addShadow(_ radius: CGFloat? = nil) {
        self.layer.shadowColor = UIColor.darkGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0.5, height: 0.5)
        self.layer.shadowRadius = 3
        self.layer.shadowOpacity = 0.25
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: radius ?? 0).cgPath
    }
}
